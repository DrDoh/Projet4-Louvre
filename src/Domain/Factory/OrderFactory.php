<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 31/08/2018
 * Time: 10:19.
 */

namespace App\Domain\Factory;

use App\Domain\Factory\Interfaces\OrderFactoryInterfaces;
use App\Domain\Models\Interfaces\OrderInterface;
use App\Domain\Models\Order;

final class OrderFactory implements OrderFactoryInterfaces
{
    /**
     * {@inheritdoc}
     */
    public function createFromUI($order): OrderInterface
    {
        return new Order(
            $order->creationDate,
            $order->email,
            $order->orderId,
            $order->visitDate,
            $order->visitRange,
            $tickets = null
        );
    }
}
