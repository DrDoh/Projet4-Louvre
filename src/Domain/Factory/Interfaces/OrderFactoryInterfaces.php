<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 31/08/2018
 * Time: 10:23.
 */

namespace App\Domain\Factory\Interfaces;

use App\Domain\Models\Interfaces\OrderInterface;

interface OrderFactoryInterfaces
{
    /**
     * @param $order
     *
     * @return OrderInterface
     */
    public function createFromUI($order): OrderInterface;
}
