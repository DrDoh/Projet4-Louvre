<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 20/09/2018
 * Time: 11:41.
 */

namespace App\Domain\Factory\Interfaces;

use App\Domain\Models\Interfaces\TicketInterface;

interface TicketFactoryInterface
{
    /**
     * @param $tickets
     *
     * @return TicketInterface
     */
    public function createFromUI($tickets);
}
