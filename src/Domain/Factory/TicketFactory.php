<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 20/09/2018
 * Time: 11:40.
 */

namespace App\Domain\Factory;

use App\Domain\Factory\Interfaces\TicketFactoryInterface;
use App\Domain\Models\Ticket;

final class TicketFactory implements TicketFactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function createFromUI($tickets)
    {
        foreach ($tickets as $key => $ticket) {
            $ticket = new Ticket(
                $ticket->birthdate,
                $ticket->creationDate,
                $ticket->country,
                $ticket->firstName,
                $ticket->lastName,
                $ticket->price,
                $ticket->ticketId,
                $ticket->type
            );

            $tickets[$key] = $ticket;
        }

        return $tickets;
    }
}
