<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 03/09/2018
 * Time: 14:06.
 */

namespace App\Domain\DTO;

use App\Domain\DTO\interfaces\TicketDTOInterface;
use DateTimeInterface;
use Ramsey\Uuid\Uuid;

/**
 * Class TicketDTO.
 */
final class TicketDTO implements TicketDTOInterface
{
    /**
     * @var DateTimeInterface
     */
    public $birthdate;

    /**
     * @var string
     */
    public $country;

    /**
     * @var string
     */
    public $discount;

    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    public $lastName;

    /**
     * @var string
     */
    public $ticketId;

    /**
     * @var DateTimeInterface
     */
    public $creationDate;

    /**
     * @var string
     */
    public $type = null;

    /**
    /**
     * @var int
     */
    public $price = null;

    /**
     *{@inheritdoc}
     */
    public function __construct(
        DateTimeInterface $birthdate,
        string $country,
        string $discount,
        string $firstName,
        string $lastName
    ) {
        $this->lastName = $lastName;
        $this->firstName = $firstName;
        $this->birthdate = $birthdate;
        $this->discount = $discount;
        $this->country = $country;
        $this->creationDate = new \DateTime();
        $this->ticketId = Uuid::uuid1()->toString();
    }
}
