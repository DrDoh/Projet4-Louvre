<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 03/09/2018
 * Time: 14:10.
 */

namespace App\Domain\DTO\interfaces;

use DateTimeInterface;

interface TicketDTOInterface
{
    /**
     * TicketDTOInterface constructor.
     *
     * @param DateTimeInterface $birthdate
     * @param string            $country
     * @param string            $discount
     * @param string            $firstName
     * @param string            $lastName
     */
    public function __construct(
        DateTimeInterface $birthdate,
        string $country,
        string $discount,
        string $firstName,
        string $lastName
    );
}
