<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 31/08/2018
 * Time: 12:09.
 */

namespace App\Domain\DTO\interfaces;

interface OrderDTOInterface
{
    /**
     * OrderDTOInterface constructor.
     *
     * @param \DateTimeInterface $visitDate
     * @param string             $visitRange
     * @param array              $tickets
     * @param string             $email
     */
    public function __construct(
        \DateTimeInterface $visitDate,
        string $visitRange,
        array $tickets = [],
        string $email
    );
}
