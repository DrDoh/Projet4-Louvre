<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 31/08/2018
 * Time: 12:09.
 */

namespace App\Domain\DTO;

use App\Domain\DTO\interfaces\OrderDTOInterface;
use DateTimeInterface;
use Ramsey\Uuid\Uuid;

final class OrderDTO implements OrderDTOInterface
{
    /**
     * @var DateTimeInterface
     */
    public $creationDate;

    /**
     * @var Array\null
     */
    public $email = null;

    /**
     * @var string
     */
    public $orderId;

    /**
     * @var array
     */
    public $tickets = [];

    /**
     * @var DateTimeInterface
     */
    public $visitDate;

    /**
     * @var string
     */
    public $visitRange;

    /**
     * {@inheritdoc}
     */
    public function __construct(
        DateTimeInterface $visitDate,
        string $visitRange,
        array $tickets = [],
        string $email = null
    ) {
        $this->creationDate = new \DateTime();
        $this->email = $email;
        $this->orderId = Uuid::uuid1()->toString();
        $this->tickets = $tickets;
        $this->visitDate = $visitDate;
        $this->visitRange = $visitRange;
    }
}
