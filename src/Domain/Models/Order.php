<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 27/08/2018
 * Time: 18:03.
 */

namespace App\Domain\Models;

use App\Domain\Models\Interfaces\OrderInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * Class Order.
 */
class Order implements OrderInterface
{
    /**
     * @var \DateTimeInterface
     */
    private $creationDate;

    /**
     * @var string
     */
    private $email;

    /**
     * @var UuidInterface
     */
    private $id;

    /**
     * @var string
     */
    private $orderId;

    /**
     * @var \ArrayAccess
     */
    private $tickets;

    /**
     * @var \DateTimeInterface
     */
    private $visitDate;

    /**
     * @var string
     */
    private $visitRange;

    /**
     * Order constructor.
     *
     * @param string             $email
     * @param string             $orderId
     * @param \DateTimeInterface $creationDate
     * @param \DateTimeInterface $visitDate
     * @param string             $visitRange
     *
     * @throws \Exception
     */
    public function __construct(
        \DateTimeInterface $creationDate,
        string $email,
        string $orderId,
        \DateTimeInterface $visitDate,
        string $visitRange
    ) {
        $this->creationDate = $creationDate;
        $this->email = $email;
        $this->id = Uuid::uuid4();
        $this->orderId = $orderId;
        $this->visitDate = $visitDate;
        $this->visitRange = $visitRange;
    }

    /**
     * {@inheritdoc}
     */
    public function addTickets($tickets)
    {
        $this->tickets = new ArrayCollection($tickets);
    }

    /**
     * {@inheritdoc}
     */
    public function getCreationDate(): \DateTimeInterface
    {
        return $this->creationDate;
    }

    /**
     * {@inheritdoc}
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * {@inheritdoc}
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getOrderId(): string
    {
        return $this->orderId;
    }

    /**
     * {@inheritdoc}
     */
    public function getTickets(): \ArrayAccess
    {
        return $this->tickets;
    }

    /**
     * {@inheritdoc}
     */
    public function getVisitDate(): \DateTimeInterface
    {
        return $this->visitDate;
    }

    /**
     * {@inheritdoc}
     */
    public function getVisitRange(): string
    {
        return $this->visitRange;
    }
}
