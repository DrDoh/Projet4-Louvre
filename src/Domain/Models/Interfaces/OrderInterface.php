<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 27/08/2018
 * Time: 18:50.
 */

namespace App\Domain\Models\Interfaces;

use DateTimeInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * Interface OrderInterface.
 */
interface OrderInterface
{
    /**
     * @param $tickets
     *
     * @return mixed
     */
    public function addTickets($tickets);

    /**
     * @return DateTimeInterface
     */
    public function getCreationDate(): DateTimeInterface;

    /**
     * @return string
     */
    public function getEmail(): string;

    /**
     * @return string
     */
    public function getOrderId(): string;

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface;

    /**
     * @return \ArrayAccess
     */
    public function getTickets();

    /**
     * @return DateTimeInterface
     */
    public function getVisitDate(): DateTimeInterface;

    /**
     * @return string
     */
    public function getVisitRange(): string;
}
