<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 27/08/2018
 * Time: 18:51.
 */

namespace App\Domain\Models\Interfaces;

use DateTimeInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * Interface TicketInterface.
 */
interface TicketInterface
{
    /**
     * @return DateTimeInterface
     */
    public function getBirthdate(): DateTimeInterface;

    /**
     * @return string
     */
    public function getCountry(): string;

    /**
     * @return DateTimeInterface
     */
    public function getCreationDate(): DateTimeInterface;

    /**
     * @return string
     */
    public function getFirstName(): string;

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface;

    /**
     * @return string
     */
    public function getLastName(): string;

    /**
     * @return int
     */
    public function getPrice(): int;

    /**
     * @return string
     */
    public function getTicketId(): string;

    /**
     * @return string
     */
    public function getTicketType(): string;
}
