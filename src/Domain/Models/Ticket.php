<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 27/08/2018
 * Time: 18:02.
 */

namespace App\Domain\Models;

use App\Domain\Models\Interfaces\TicketInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * Class Ticket.
 */
class Ticket implements TicketInterface
{
    /**
     * @var \DateTimeInterface
     */
    private $birthdate;

    /**
     * @var string
     */
    private $country;

    /**
     * @var \DateTimeInterface
     */
    private $creationDate;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var UuidInterface
     */
    private $id;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var int
     */
    private $price;

    /**
     * @var string
     */
    private $ticketId;
    /**
     * @var string
     */
    private $ticketType;

    /**
     * Ticket constructor.
     *
     * @param \DateTimeInterface $birthdate
     * @param string             $country
     * @param \DateTimeInterface $creationDate
     * @param string             $firstName
     * @param string             $lastName
     * @param int                $price
     * @param string             $ticketId
     * @param string             $ticketType
     *
     * @throws \Exception
     */
    public function __construct(
        \DateTimeInterface $birthdate,
        \DateTimeInterface $creationDate,
        string $country,
        string $firstName,
        string $lastName,
        int $price,
        string $ticketId,
        string $ticketType
    ) {
        $this->birthdate = $birthdate;
        $this->country = $country;
        $this->creationDate = $creationDate;
        $this->firstName = $firstName;
        $this->id = Uuid::uuid4();
        $this->lastName = $lastName;
        $this->price = $price;
        $this->ticketId = $ticketId;
        $this->ticketType = $ticketType;
    }

    /**
     * {@inheritdoc}
     */
    public function getBirthdate(): \DateTimeInterface
    {
        return $this->birthdate;
    }

    /**
     * {@inheritdoc}
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * {@inheritdoc}
     */
    public function getCreationDate(): \DateTimeInterface
    {
        return $this->creationDate;
    }

    /**
     * {@inheritdoc}
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * {@inheritdoc}
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * {@inheritdoc}
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * {@inheritdoc}
     */
    public function getTicketId(): string
    {
        return $this->ticketId;
    }

    /**
     * {@inheritdoc}
     */
    public function getTicketType(): string
    {
        return $this->ticketType;
    }
}
