<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 28/08/2018
 * Time: 17:20.
 */

namespace App\UI\Form;

use App\Domain\DTO\interfaces\TicketDTOInterface;
use App\Domain\DTO\TicketDTO;
use App\UI\Form\Interfaces\TicketTypeInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TicketType.
 */
final class TicketType extends AbstractType implements TicketTypeInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class,[
                'label' => 'booking.firstName',
            ])
            ->add('lastName', TextType::class,[
                'label' => 'booking.lastName',
            ])
            ->add('birthdate', DateType::class, [
                    'widget' => 'single_text',
                    'html5' => false,
                    'input' => 'datetime',
                    'format' => 'dd/MM/yyyy',
                    'label' => 'booking.birthdate',
                    'attr' => ['class' => 'js-datepicker'],
                ]
            )
            ->add('discount', ChoiceType::class, [
                    'label' => 'booking.discount',
                    'label_attr'=>[
                        'title' => 'booking.warning.message',
                        'data-toggle'=>'tooltip',
                        'data-placement'=>'right',
                    ],
                    'choices' => [
                        '' => 'none',
                        'booking.student' => 'etude',
                        'booking.employee' => 'employee',
                        'booking.military' => 'military',
                        'booking.ministary' => 'ministary',
                        ],
                ]
            )
            ->add('country', CountryType::class, [
                'label' => 'booking.country',
                'preferred_choices' => [
                    'EN', 'FR',
                ],
                'choice_translation_locale' =>null,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TicketDTOInterface::class,
            'empty_data' => function (FormInterface $form) {
                return new TicketDTO(
                    $form->get('birthdate')->getData(),
                    $form->get('country')->getData(),
                    $form->get('discount')->getData(),
                    $form->get('firstName')->getData(),
                    $form->get('lastName')->getData()
                );
            },
            'validation_groups' => ['ticketTypeCreation'],
        ]);
    }
}
