<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 31/08/2018
 * Time: 09:46.
 */

namespace App\UI\Form;

use App\Domain\DTO\interfaces\OrderDTOInterface;
use App\Domain\DTO\OrderDTO;
use App\UI\Form\Interfaces\OrderTypeInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class OrderType.
 */
final class OrderType extends AbstractType implements OrderTypeInterface
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('visitDate', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'input' => 'datetime',
                'format' => 'dd/MM/yyyy',
                'label' => 'order.visit_date',
                'attr' => ['class' => 'js-datepicker'],
                ]
            )
            ->add('visitRange', ChoiceType::class, [
                'choices' => [
                    'order.day' => 'fullDay',
                    'order.half_day' => 'halfDay',
                    ],
                'label' => 'order.visit_range',
                'choice_attr' => function ($key) {
                    return ['id' => $key];
                },
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'label' => false,
            'data_class' => OrderDTOInterface::class,
            'empty_data' => function (FormInterface $form) {
                return new OrderDTO(
                    $form->get('visitDate')->getData(),
                    $form->get('visitRange')->getData()
                );
            },
            'validation_groups' => ['orderTypeCreation'],
        ]);
    }
}
