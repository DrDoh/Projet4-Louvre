<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 06/09/2018
 * Time: 16:20.
 */

namespace App\UI\Form;

use App\UI\Form\Interfaces\BookingTypeInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class BookingType extends AbstractType implements BookingTypeInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tickets', CollectionType::class, [
                    'entry_type' => TicketType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'label' => false,
                ]
            );
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'label' => false,
            'validation_groups' => ['ticketTypeCreation'],
        ]);
    }
}
