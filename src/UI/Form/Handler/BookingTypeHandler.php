<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 07/09/2018
 * Time: 10:32.
 */

namespace App\UI\Form\Handler;

use App\UI\Form\Handler\Interfaces\BookingTypeHandlerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

final class BookingTypeHandler implements BookingTypeHandlerInterface
{
    private $session;

    /**
     * {@inheritdoc}
     */
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * {@inheritdoc}
     */
    public function handle(FormInterface $form): bool
    {
        if ($form->isSubmitted() && $form->isValid()) {
            $this->session->get('order')->tickets = $form->get('tickets')->getData();

            return true;
        }

        return false;
    }
}
