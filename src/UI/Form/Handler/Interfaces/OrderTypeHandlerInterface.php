<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 31/08/2018
 * Time: 10:17.
 */

namespace App\UI\Form\Handler\Interfaces;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

interface OrderTypeHandlerInterface
{
    /**
     * OrderTypeHandlerInterface constructor.
     *
     * @param SessionInterface $session
     */
    public function __construct(SessionInterface $session);

    /**
     * @param FormInterface $form
     *
     * @return bool
     */
    public function handle(FormInterface $form): bool;
}
