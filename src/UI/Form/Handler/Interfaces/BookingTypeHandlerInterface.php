<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 07/09/2018
 * Time: 10:33.
 */

namespace App\UI\Form\Handler\Interfaces;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

interface BookingTypeHandlerInterface
{
    /**
     * BookingTypeHandlerInterface constructor.
     *
     * @param SessionInterface $session
     */
    public function __construct(SessionInterface $session);

    /**
     * @param FormInterface $form
     *
     * @return mixed
     */
    public function handle(FormInterface $form);
}
