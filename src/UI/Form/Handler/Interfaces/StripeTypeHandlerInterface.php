<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 17/09/2018
 * Time: 14:48.
 */

namespace App\UI\Form\Handler\Interfaces;

use App\Domain\Factory\Interfaces\OrderFactoryInterfaces;
use App\Domain\Factory\Interfaces\TicketFactoryInterface;
use App\Infra\Doctrine\Repository\Interfaces\OrderRepositoryInterface;
use App\Infra\Tools\interfaces\TicketSenderInterface;
use App\Infra\Tools\interfaces\StripeInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

interface StripeTypeHandlerInterface
{
    /**
     * StripeTypeHandlerInterface constructor.
     *
     * @param MessageBusInterface $bus
     * @param OrderFactoryInterfaces $orderFactory
     * @param OrderRepositoryInterface $orderRepository
     * @param TicketSenderInterface $sendTicket
     * @param SessionInterface $session
     * @param StripeInterface $stripe
     * @param TicketFactoryInterface $ticketFactory
     * @param ValidatorInterface $validator
     */
    public function __construct(
        MessageBusInterface $bus,
        OrderFactoryInterfaces $orderFactory,
        OrderRepositoryInterface $orderRepository,
        TicketSenderInterface $sendTicket,
        SessionInterface $session,
        StripeInterface $stripe,
        TicketFactoryInterface $ticketFactory,
        ValidatorInterface $validator
    );

    /**
     * @param Request $request
     *
     * @return bool
     */
    public function handle(Request $request): bool;
}
