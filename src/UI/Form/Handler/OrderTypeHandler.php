<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 31/08/2018
 * Time: 10:11.
 */

namespace App\UI\Form\Handler;

use App\UI\Form\Handler\Interfaces\OrderTypeHandlerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class OrderTypeHandler.
 */
final class OrderTypeHandler implements OrderTypeHandlerInterface
{
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * {@inheritdoc}
     */
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     *{@inheritdoc}
     */
    public function handle(FormInterface $form): bool
    {
        if ($form->isSubmitted() && $form->isValid()) {
            $this->session->set('order', $form->getData());

            return true;
        }

        return false;
    }
}
