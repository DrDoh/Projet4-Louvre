<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 17/09/2018
 * Time: 14:48.
 */

namespace App\UI\Form\Handler;

use App\Application\Messenger\Message\NewOrderMessage;
use App\Domain\Factory\Interfaces\OrderFactoryInterfaces;
use App\Domain\Factory\Interfaces\TicketFactoryInterface;
use App\Infra\Doctrine\Repository\Interfaces\OrderRepositoryInterface;
use App\Infra\Tools\interfaces\TicketSenderInterface;
use App\Infra\Tools\interfaces\StripeInterface;
use App\UI\Form\Handler\Interfaces\StripeTypeHandlerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class StripeTypeHandler implements StripeTypeHandlerInterface
{
    /**
     * @var MessageBusInterface
     */
    private $bus;

    /**
     * @var OrderFactoryInterfaces
     */
    private $orderFactory;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var TicketSenderInterface
     */
    private $sendTicket;

    /**
     * @var StripeInterface
     */
    private $stripe;

    /**
     * @var TicketFactoryInterface
     */
    private $ticketFactory;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * {@inheritdoc}
     */
    public function __construct(
        MessageBusInterface $bus,
        OrderFactoryInterfaces $orderFactory,
        OrderRepositoryInterface $orderRepository,
        TicketSenderInterface $sendTicket,
        SessionInterface $session,
        StripeInterface $stripe,
        TicketFactoryInterface $ticketFactory,
        ValidatorInterface $validator
    ) {
        $this->bus = $bus;
        $this->orderFactory = $orderFactory;
        $this->orderRepository = $orderRepository;
        $this->sendTicket = $sendTicket;
        $this->session = $session;
        $this->stripe = $stripe;
        $this->ticketFactory = $ticketFactory;
        $this->validator=$validator;
    }

    /**
     * {@inheritdoc}
     */
    public function handle(Request $request): bool
    {
        if ($request->request->get('email') && $request->request->get('token')) {
            $this->session->get('order')->email = $request->request->get('email');

            if (!$this->stripe->payment($this->session->get('order'), $request->request->get('token'))) {
                throw new \LogicException();
            }

            $order = $this->orderFactory->createFromUI($this->session->get('order'));
            $tickets = $this->ticketFactory->createFromUI($this->session->get('order')->tickets);
            $order->addTickets($tickets);

            $error = $this->validator->validate($order);

            if(count($error) === 0){
                $this->orderRepository->save($order);
            }

            $this->bus->dispatch(new NewOrderMessage($order));

            return true;
        }

        return false;
    }
}
