<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 03/09/2018
 * Time: 11:40.
 */

namespace App\UI\Responder;

use App\UI\Responder\Interfaces\BookingActionResponderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

final class BookingActionResponder implements BookingActionResponderInterface
{
    /**
     * @var Environment
     */
    private $env = null;

    /**
     * @var UrlGeneratorInterface|null
     */
    private $generator = null;

    /**
     * {@inheritdoc}
     */
    public function __construct(
        Environment $env,
        UrlGeneratorInterface $generator
    ) {
        $this->env = $env;
        $this->generator = $generator;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(
        Request $request,
        $redirect = false,
        FormInterface $ticketType = null
    ) {
        $redirect
            ? $response = new RedirectResponse($this->generator->generate('Payment'))
            : $response = new Response(
                $this->env->render('booking.html.twig', [
                    'form' => $ticketType->createView(),
                ])
        );

        return $response;
    }
}
