<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 04/09/2018
 * Time: 10:33.
 */

namespace App\UI\Responder;

use App\UI\Responder\Interfaces\PaymentActionResponderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

final class PaymentActionResponder implements PaymentActionResponderInterface
{
    /**
     * @var Environment
     */
    private $env = null;

    /**
     * {@inheritdoc}
     */
    public function __construct(Environment $env)
    {
        $this->env = $env;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(
        Request $request,
        $redirect = false
    ) {
        $redirect
            ? $response = new Response(
                $this->env->render('thx.html.twig'))
            : $response = new Response(
                $this->env->render('payment.html.twig')
        );

        return $response;
    }
}
