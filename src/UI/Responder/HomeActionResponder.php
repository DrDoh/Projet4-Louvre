<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 29/08/2018
 * Time: 10:38.
 */

namespace App\UI\Responder;

use App\UI\Responder\Interfaces\HomeActionResponderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

final class HomeActionResponder implements HomeActionResponderInterface
{
    /**
     * @var Environment
     */
    private $env = null;


    /**
     * @var UrlGeneratorInterface|null
     */
    private $generator = null;


    /**
     * {@inheritdoc}
     */
    public function __construct(
        Environment $env,
        UrlGeneratorInterface $generator
    ) {
        $this->env = $env;
        $this->generator =$generator;

    }

    /**
     *{@inheritdoc}
     */
    public function __invoke(
        Request $request,
        $redirect = false,
        FormInterface $form = null,
        $unavailableDate = null
    ): Response {
        $redirect
            ? $response = new RedirectResponse($this->generator->generate('Booking'))
            : $response = new Response(
                $this->env->render('home.html.twig', [
                    'form' => $form->createView(),
                    'unavailableDate' => $unavailableDate
                ])
        );

        return $response;
    }
}
