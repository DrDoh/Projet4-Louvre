<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 04/09/2018
 * Time: 10:33.
 */

namespace App\UI\Responder\Interfaces;

use Symfony\Component\HttpFoundation\Request;
use Twig\Environment;

interface PaymentActionResponderInterface
{
    /**
     * PaymentActionResponderInterface constructor.
     *
     * @param Environment $env
     */
    public function __construct(Environment $env);

    /**
     * @param Request $request
     * @param bool    $redirect
     *
     * @return mixed
     */
    public function __invoke(
        Request $request,
        $redirect = false
    );
}
