<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 03/09/2018
 * Time: 11:40.
 */

namespace App\UI\Responder\Interfaces;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

interface BookingActionResponderInterface
{
    /**
     * BookingActionResponderInterface constructor.
     *
     * @param Environment           $env
     * @param UrlGeneratorInterface $generator
     */
    public function __construct(
        Environment $env,
        UrlGeneratorInterface $generator
    );

    /**
     * @param Request       $request
     * @param bool          $redirect
     * @param FormInterface $ticketType
     *
     * @return
     */
    public function __invoke(
        Request $request,
        $redirect = false,
        FormInterface $ticketType
    );
}
