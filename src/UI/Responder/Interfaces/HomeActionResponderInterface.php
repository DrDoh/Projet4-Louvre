<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 29/08/2018
 * Time: 10:41.
 */

namespace App\UI\Responder\Interfaces;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

/**
 * Interface HomeActionResponderInterface.
 */
interface HomeActionResponderInterface
{
    /**
     * HomeActionResponderInterface constructor.
     *
     * @param Environment $env
     * @param UrlGeneratorInterface $generator
     */
    public function __construct(
        Environment $env,
        UrlGeneratorInterface $generator
    );

    /**
     * @param Request $request
     * @param bool $redirect
     * @param FormInterface|null $form
     *
     * @param null $unavailableDate
     * @return Response
     */
    public function __invoke(
        Request $request,
        $redirect = false,
        FormInterface $form = null,
        $unavailableDate = null
    ): Response;
}
