<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 03/09/2018
 * Time: 10:46.
 */

namespace App\UI\Action;

use App\Infra\Tools\interfaces\AvailabilityCheckerInterface;
use App\UI\Action\Interfaces\BookingActionInterface;
use App\UI\Form\BookingType;
use App\UI\Form\Handler\Interfaces\BookingTypeHandlerInterface;
use App\UI\Responder\Interfaces\BookingActionResponderInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(
 *     name="Booking",
 *     path="/{_locale}/billetterie",
 *     requirements={
 *         "_locale": "en|fr"
 *     },
 *     methods={"GET","POST"}
 * )
 */
final class BookingAction implements BookingActionInterface
{
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var BookingTypeHandlerInterface
     */
    private $formHandler;

    /**
     * @var AvailabilityCheckerInterface
     */
    private $availabilityChecker;

    /**
     * {@inheritdoc}
     */
    public function __construct(
        FormFactoryInterface $formFactory,
        BookingTypeHandlerInterface $typeHandler,
        AvailabilityCheckerInterface $availabilityChecker
    ) {
        $this->formFactory = $formFactory;
        $this->formHandler = $typeHandler;
        $this->availabilityChecker = $availabilityChecker;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(
        Request $request,
        BookingActionResponderInterface $responder
    ): Response {

        if (!$request->getSession()->get('order')) {
            throw new \LogicException();
        }

        $type = $this->formFactory->create(BookingType::class, ['tickets'=>$request->getSession()->get('order')->tickets])
                                  ->handleRequest($request);

        if ($this->formHandler->handle($type)) {
            return $responder($request, true, $type);
        }

        return $responder($request, false, $type);
    }
}
