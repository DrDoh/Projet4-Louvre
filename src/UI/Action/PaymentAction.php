<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 04/09/2018
 * Time: 10:22.
 */

namespace App\UI\Action;

use App\Infra\Tools\PriceCalculator;
use App\UI\Action\Interfaces\PaymentActionInterface;
use App\UI\Form\Handler\Interfaces\StripeTypeHandlerInterface;
use App\UI\Responder\Interfaces\PaymentActionResponderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(
 *     name="Payment",
 *     path="/{_locale}/payement",
 *     requirements={
 *         "_locale": "en|fr"
 *     },
 *     methods={"GET","POST"}
 * )
 */
final class PaymentAction implements PaymentActionInterface
{
    /**
     * @var StripeTypeHandlerInterface
     */
    private $formHandler;

    /**
     * {@inheritdoc}
     */
    public function __construct(StripeTypeHandlerInterface $formHandler)
    {
        $this->formHandler = $formHandler;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(
        Request $request,
        PaymentActionResponderInterface $responder,
        PriceCalculator $priceCalculator
    ) {
        if (!$request->getSession()->get('order')) {
            throw new \LogicException();
        }

        $order = $request->getSession()->get('order');

        $order->total = $priceCalculator->updateOrderDTO($order);

        if ($request->isMethod('POST') && $this->formHandler->handle($request)) {
            return $responder($request, true);
        }

        return $responder($request, false);
    }
}
