<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Ludo
 * Date: 24/08/2018
 * Time: 11:36.
 */

namespace App\UI\Action;

use App\Infra\Tools\interfaces\AvailabilityCheckerInterface;
use App\UI\Action\Interfaces\HomeActionInterface;
use App\UI\Form\OrderType;
use App\UI\Form\Handler\Interfaces\OrderTypeHandlerInterface;
use App\UI\Responder\Interfaces\HomeActionResponderInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(
 *     name="Home",
 *     path="/{_locale}",
 *     defaults={"_locale"="fr"},
 *     requirements={
 *         "_locale": "en|fr"
 *     },
 *     methods={"GET","POST"}
 * )
 */
final class HomeAction implements HomeActionInterface
{
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var OrderTypeHandlerInterface
     */
    private $formHandler;

    /**
     * @var AvailabilityCheckerInterface
     */
    private $availabilityChecker;

    /**
     * {@inheritdoc}
     */
    public function __construct(
        FormFactoryInterface $formFactory,
        OrderTypeHandlerInterface $typeHandler,
        AvailabilityCheckerInterface $availabilityChecker
    ) {
        $this->formFactory = $formFactory;
        $this->formHandler = $typeHandler;
        $this->availabilityChecker = $availabilityChecker;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(
        Request $request,
        HomeActionResponderInterface $responder
    ): Response {
        if ($request->getSession()->has('order')) {
            $request->getSession()->clear();
        }

        $unavailableDate = $this->availabilityChecker->getUnavailableDate();

        $type = $this->formFactory->create(OrderType::class)
                                  ->handleRequest($request);

        if ($this->formHandler->handle($type)) {
            return $responder($request, true);
        }

        return $responder($request, false, $type, $unavailableDate);
    }
}
