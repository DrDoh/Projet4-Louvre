<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 24/08/2018
 * Time: 15:01.
 */

namespace App\UI\Action\Interfaces;

use App\Infra\Tools\interfaces\AvailabilityCheckerInterface;
use App\UI\Form\Handler\Interfaces\OrderTypeHandlerInterface;
use App\UI\Responder\Interfaces\HomeActionResponderInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface HomeActionInterface
{
    /**
     * HomeActionInterface constructor.
     *
     * @param FormFactoryInterface $formFactory
     * @param OrderTypeHandlerInterface $typeHandler
     * @param AvailabilityCheckerInterface $availabilityChecker
     */
    public function __construct(
        FormFactoryInterface $formFactory,
        OrderTypeHandlerInterface $typeHandler,
        AvailabilityCheckerInterface $availabilityChecker
    );

    /**
     * @param Request                      $request
     * @param HomeActionResponderInterface $homeResponder
     *
     * @return Response
     */
    public function __invoke(
        Request $request,
        HomeActionResponderInterface $homeResponder
    ): Response;
}
