<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 03/09/2018
 * Time: 10:46.
 */

namespace App\UI\Action\Interfaces;

use App\Infra\Tools\interfaces\AvailabilityCheckerInterface;
use App\UI\Form\Handler\Interfaces\BookingTypeHandlerInterface;
use App\UI\Responder\Interfaces\BookingActionResponderInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface BookingActionInterface
{
    /**
     * BookingActionInterface constructor.
     *
     * @param FormFactoryInterface $formFactory
     * @param BookingTypeHandlerInterface $typeHandler
     * @param AvailabilityCheckerInterface $availabilityChecker
     */
    public function __construct(
        FormFactoryInterface $formFactory,
        BookingTypeHandlerInterface $typeHandler,
        AvailabilityCheckerInterface $availabilityChecker
    );

    /**
     * @param Request                         $request
     * @param BookingActionResponderInterface $responder
     *
     * @return Response
     */
    public function __invoke(
        Request $request,
        BookingActionResponderInterface $responder
    ): Response;
}
