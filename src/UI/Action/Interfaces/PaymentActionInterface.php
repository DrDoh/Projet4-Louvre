<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 04/09/2018
 * Time: 10:23.
 */

namespace App\UI\Action\Interfaces;

use App\Infra\Tools\PriceCalculator;
use App\UI\Form\Handler\Interfaces\StripeTypeHandlerInterface;
use App\UI\Responder\Interfaces\PaymentActionResponderInterface;
use Symfony\Component\HttpFoundation\Request;

interface PaymentActionInterface
{
    /**
     * PaymentActionInterface constructor.
     *
     * @param StripeTypeHandlerInterface $typeHandler
     */
    public function __construct(StripeTypeHandlerInterface $typeHandler);

    /**
     * @param Request                         $request
     * @param PaymentActionResponderInterface $responder
     * @param PriceCalculator                 $priceCal
     *
     * @return mixed
     */
    public function __invoke(
        Request $request,
        PaymentActionResponderInterface $responder,
        PriceCalculator $priceCal
    );
}
