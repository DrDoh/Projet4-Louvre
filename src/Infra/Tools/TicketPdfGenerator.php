<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 11/10/2018
 * Time: 16:18
 */

namespace App\Infra\Tools;


use App\Application\Messenger\Message\Interfaces\NewOrderMessageInterface;
use App\Infra\Tools\interfaces\TicketPdfGeneratorInterface;
use Spipu\Html2Pdf\Html2Pdf;
use Twig\Environment;

final class TicketPdfGenerator implements TicketPdfGeneratorInterface
{
    /**
     * @var Environment
     */
    private $env;

    /**
     * TicketPdfGenerator constructor.
     *
     * @param Environment $env
     */
    public function __construct(Environment $env)
    {
        $this->env = $env;
    }

    /**
     * {@inheritdoc}
     */
    public function createPdf( NewOrderMessageInterface $message)
    {

        $html2pdf = new Html2Pdf('P', 'A4', 'fr',true, 'UTF-8',[10, 10, 10, 10]);

        $html2pdf->writeHTML(
            $this->env->render('Pdf/ticket_pdf.html.twig',[
                'order'=> $message->getOrder()
                ]
            )
        );

        Return $html2pdf->output('Le Louvre : Billet d\'accées.pdf','S');
    }

}