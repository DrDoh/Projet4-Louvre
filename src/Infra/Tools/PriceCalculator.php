<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 13/09/2018
 * Time: 15:23.
 */

namespace App\Infra\Tools;

use App\Infra\Tools\interfaces\PriceCalculatorInterface;

/**
 * Class PriceCalculator.
 */
final class PriceCalculator implements PriceCalculatorInterface
{
    /**
     * @var int
     */
    private $babyPrice;

    /**
     * @var int
     */
    private $childMaxAge;

    /**
     * @var int
     */
    private $childMinAge;

    /**
     * @var int
     */
    private $childPrice;

    /**
     * @var int
     */
    private $discountPrice;

    /**
     * @var int
     */
    private $fullPrice;

    /**
     * @var int
     */
    private $seniorMinAge;

    /**
     * @var int
     */
    private $seniorPrice;

    /**
     * {@inheritdoc}
     */
    public function __construct(
        $fullPrice,
        $childPrice,
        $babyPrice,
        $seniorPrice,
        $discountPrice,
        $childMaxAge,
        $childMinAge,
        $seniorMinAge
    ) {
        $this->babyPrice = $babyPrice;
        $this->childMinAge = $childMinAge;
        $this->childMaxAge = $childMaxAge;
        $this->childPrice = $childPrice;
        $this->discountPrice = $discountPrice;
        $this->fullPrice = $fullPrice;
        $this->seniorPrice = $seniorPrice;
        $this->seniorMinAge = $seniorMinAge;
    }

    /**
     * @param $discount
     *
     * @return array [string $priceType, int $price]
     */
    private function selectDiscountType($discount)
    {
        switch ($discount) {
            case 'employee':
                $discountPrice = $this->discountPrice;
                $priceType = 'Employé';
                break;
            case 'etude':
                $discountPrice = $this->discountPrice;
                $priceType = 'Etudiant';
                break;
            case 'military':
                $discountPrice = $this->discountPrice;
                $priceType = 'Militaire';
                break;
            case 'ministary':
                $discountPrice = $this->discountPrice;
                $priceType = 'Membre du ministaire de la culture';
                break;
            default:
                $discountPrice = $this->fullPrice;
                $priceType = 'Normal';
        }

        return $discountArray = ['type' => $priceType, 'price' => $discountPrice];
    }

    /**
     * @param $age
     *
     * @return array [string $priceType, int $price]
     */
    private function selectAgeType($age)
    {
        $age = intval($age);
        switch (true) {
            case ($age <= $this->childMinAge) :
                $agePrice = $this->babyPrice;
                $priceType = 'Bébé';
                break;

            case ($age > $this->childMinAge && $age <= $this->childMaxAge) :
                $agePrice = $this->childPrice;
                $priceType = 'Enfant';
                break;

            case ($age >= $this->seniorMinAge) :
                $agePrice = $this->seniorPrice;
                $priceType = 'Senior';
                break;

            default:
                $agePrice = $this->fullPrice;
                $priceType = 'Normal';
        }


        return $ageArray = ['type' => $priceType, 'price' => $agePrice];
    }

    /**
     * @param $order
     *
     * @return float|int
     */
    private function getTotalPrice($order)
    {
        $prices = [];
        $tickets = $order->tickets;

        foreach ($tickets as $ticket) {
            $prices[] = $ticket->price;
        }

        $total = array_sum($prices);

        return $total;
    }

    /**
     * {@inheritdoc}
     */
    public function updateOrderDTO($order)
    {
        $tickets = $order->tickets;

        foreach ($tickets as $ticket) {
            $age = $order->visitDate->diff($ticket->birthdate)->format('%y');

            $discountPrice = $this->selectDiscountType($ticket->discount);
            $agePrice = $this->selectAgeType($age);

            $price = $agePrice;

            if ($discountPrice['price'] < $agePrice['price']) {
                $price = $discountPrice;
            }

            $ticket->type = $price['type'];
            $ticket->price = $price['price'];
        }

        return $this->getTotalPrice($order);
    }
}
