<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 13/09/2018
 * Time: 15:25.
 */

namespace App\Infra\Tools;

use App\Infra\Tools\interfaces\TicketStatusInterface;

/**
 * Class TicketStatus.
 */
final class TicketStatus implements TicketStatusInterface
{
    private $qteMax;
    private $yearMax;

    /**
     * {@inheritdoc}
     */
    public function __construct($qteMax, $yearMax)
    {
        $this->qteMax = $qteMax;
        $this->yearMax = $yearMax;
    }

    /**
     * {@inheritdoc}
     */
    public function getSoldTickets($tickets)
    {
        $soldTicketsArray = [];

        foreach ($tickets as $ticket) {
            $ticketDate = $ticket->getDate()->format('Y-m-d');

            if (array_key_exists($ticketDate, $soldTicketsArray)) {
                ++$soldTicketsArray[$ticketDate];
            } else {
                $soldTicketsArray[$ticketDate] = 1;
            }
        }

        return $soldTicketsArray;
    }

    /**
     * {@inheritdoc}
     */
    public function getHolidays()
    {
        $N = date('Y');
        $holidays = [];
        $holidaysArray = [];

        for ($N; $N <= 2020; ++$N) {
            $Pr_mai = $N.'-05-01';
            $Pr_novembre = $N.'-11-01';
            $noel = $N.'-12-25';

            if (null === $holidays) {
                $holidays = array($Pr_mai, $Pr_novembre, $noel);
            } else {
                $holidaysArray = array_push($holidays, $Pr_mai, $Pr_novembre, $noel);
            }
        }

        return $holidays;
    }

    /**
     * {@inheritdoc}
     */
    public function getFullDate($tickets)
    {
        $holidaysArray = $this->getHolidays($this->yearMax);
        $soldTickets = $this->getSoldTickets($tickets);
        $fullDatesArray = [];
        foreach ($soldTickets as $date => $soldTicket) {
            if (0 === ($this->qteMax - $soldTicket)) {
                $fullDatesArray[] = $date;
            }
        }
        $fullDatesArray = array_merge_recursive($holidaysArray, $fullDatesArray);

        return $fullDatesArray;
    }

    /**
     * {@inheritdoc}
     */
    public function checkDispo($tickets, $userChoices)
    {
        $SoldTickets = $this->getSoldTickets($tickets);

        $dispo = true;
        $date = \DateTime::createFromFormat('d/m/Y', $userChoices['date'])->format('Y-m-d');

        if (true == array_key_exists($date, $SoldTickets)) {
            if ($SoldTickets[$date] + $userChoices['ticket_qte'] > $this->qteMax) {
                $dispo = false;
            }
        }

        return $dispo;
    }
}
