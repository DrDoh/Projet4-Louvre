<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 13/09/2018
 * Time: 15:25.
 */

namespace App\Infra\Tools;

use App\Infra\Tools\interfaces\StripeInterface;

/**
 * Class Stripe.
 */
final class Stripe implements StripeInterface
{
    private $secretKey;

    /**
     * {@inheritdoc}
     */
    public function __construct($stripeSecretKey)
    {
        $this->secretKey = $stripeSecretKey;
        \Stripe\Stripe::setApiKey($this->secretKey);
    }

    /**
     * {@inheritdoc}
     */
    public function payment($order, $token): bool
    {
        if (0 === $order->total) {
            return true;
        }

        $customer = \Stripe\Customer::create(array(
            'email' => $order->email,
            'source' => $token,
        ));

        $charge = \Stripe\Charge::create(array(
            'customer' => $customer->id,
            'amount' => $order->total * 100,
            'currency' => 'eur',
        ));

        return 'succeeded' === $charge->status;
    }
}
