<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 13/12/2018
 * Time: 15:59
 */

namespace App\Infra\Tools\interfaces;


interface TicketStatusInterface
{
    /**
     * TicketStatusInterface constructor.
     * @param $qteMax
     * @param $yearMax
     */
    public function __construct($qteMax, $yearMax);

    /**
     * Récupère le nombre de billets vendus par date.
     *
     * @param $tickets
     * @return array $soldTicketsArray 'date'=>'val'
     */
    public function getSoldTickets($tickets);

    /**
     * Récupère les jours feriés.
     *
     * @return array $holidaysArray
     */
    public function getHolidays();

    /**
     * Récupère les dates complètes.
     *
     * @param $tickets
     * @return array $fullDatesArray
     */
    public function getFullDate($tickets);

    /**
     * Verifie la dispo.
     *
     * @return bool $fullDatesArray
     */
    public function checkDispo($tickets, $userChoices);
}