<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 14/09/2018
 * Time: 11:27.
 */

namespace App\Infra\Tools\interfaces;

interface StripeInterface
{
    /**
     * StripeInterface constructor.
     *
     * @param $StripeSecretKey
     */
    public function __construct($StripeSecretKey);

    /**
     * @param $order
     * @param $token
     *
     * @return mixed
     */
    public function payment($order, $token): bool;
}
