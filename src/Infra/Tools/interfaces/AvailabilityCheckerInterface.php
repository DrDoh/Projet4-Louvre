<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 18/10/2018
 * Time: 09:57
 */

namespace App\Infra\Tools\interfaces;


interface AvailabilityCheckerInterface
{
    /**
     * @return mixed
     */
    public function getDaysOff();

    /**
     * @return mixed
     */
    public function getUnavailableDate();

    /**
     * @param $tickets
     * @return mixed
     */
    public function checkAvailability($tickets);
}