<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 11/10/2018
 * Time: 16:23
 */

namespace App\Infra\Tools\interfaces;


use App\Application\Messenger\Message\Interfaces\NewOrderMessageInterface;
use Twig\Environment;

interface TicketPdfGeneratorInterface
{
    /**
     * TicketPdfGenerator constructor.
     *
     * @param Environment $env
     */
    public function __construct(Environment $env);

    /**
     * @param NewOrderMessageInterface $message
     * @return string
     * @throws \Spipu\Html2Pdf\Exception\Html2PdfException
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function createPdf( NewOrderMessageInterface $message);

}