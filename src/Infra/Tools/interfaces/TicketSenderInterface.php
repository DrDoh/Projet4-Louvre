<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 25/09/2018
 * Time: 11:36.
 */

namespace App\Infra\Tools\interfaces;

use Swift_Mailer;
use Twig\Environment;

interface TicketSenderInterface
{
    /**
     * TicketSenderInterface constructor.
     *
     * @param Environment $env
     * @param Swift_Mailer $swift_Mailer
     * @param string $publicFolder
     */
    public function __construct(
        Environment $env,
        Swift_Mailer $swift_Mailer,
        string $publicFolder
    );

    /**
     * @param $order
     *
     * @param $pdf
     * @return mixed
     */
    public function sendByEmail($order, $pdf);
}
