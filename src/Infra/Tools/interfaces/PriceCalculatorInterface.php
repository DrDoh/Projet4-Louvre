<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 13/09/2018
 * Time: 16:00.
 */

namespace App\Infra\Tools\interfaces;

interface PriceCalculatorInterface
{
    /**
     * PriceCalculatorInterface constructor.
     *
     * @param $fullPrice
     * @param $childPrice
     * @param $babyPrice
     * @param $seniorPrice
     * @param $discountPrice
     * @param $childMaxAge
     * @param $childMinAge
     * @param $seniorMinAge
     */
    public function __construct(
        $fullPrice,
        $childPrice,
        $babyPrice,
        $seniorPrice,
        $discountPrice,
        $childMaxAge,
        $childMinAge,
        $seniorMinAge
    );

    /**
     * Set type & price in order (session) and return Total Price.
     *
     * @param $order
     *
     * @return int totalPrice
     */
    public function updateOrderDTO($order);
}
