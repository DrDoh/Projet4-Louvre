<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 18/10/2018
 * Time: 09:53
 */

namespace App\Infra\Tools;


use App\Infra\Doctrine\Repository\Interfaces\OrderRepositoryInterface;
use App\Infra\Tools\interfaces\AvailabilityCheckerInterface;

final class AvailabilityChecker implements AvailabilityCheckerInterface
{
    /**
     * @var int
     */
    private $qteMax;

    /**
     * @var int
     */
    private $yearMax;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * {@inheritdoc}
     */
    public function __construct(
        $ticketQteMax,
        $ticketYearMax,
        OrderRepositoryInterface $orderRepository
    ) {
        $this->qteMax = $ticketQteMax;
        $this->yearMax = $ticketYearMax;
        $this->orderRepository= $orderRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function getDaysOff(){

        $N= date('Y');
        $daysOff = [];
        $holidaysArray = [];

        for($N ; $N <= $this->yearMax ; $N++){

            $Pr_mai = '01/05/'.$N;
            $Pr_novembre = '01/11/'.$N;
            $noel = '25/12/'.$N;

            if($daysOff === null){
                $daysOff = array ($Pr_mai,$Pr_novembre, $noel);
            }else{
                $holidaysArray = array_push($daysOff,$Pr_mai,$Pr_novembre,$noel);
            }
        }

        return $daysOff;
    }

    /**
     * {@inheritdoc}
     */
    public function getUnavailableDate(){

        $orders = $this->orderRepository->getOrdersAfterNow();

        $ticketSold = [];
        $fullDates = [];

        foreach ($orders as $order){
            $visitDate = $order->getVisitDate()->format('d/m/Y');
            $tickets = $order->getTickets()->getValues();
            $ticketsQte = count($tickets);

            if(array_key_exists($visitDate, $ticketSold)){
                $ticketSold [$visitDate] += $ticketsQte;
            } else {
                $ticketSold [$visitDate] = $ticketsQte;
            }
        }

        foreach($ticketSold as $date => $qte){

            if(($this->qteMax - $qte) === 0){
                $fullDates[] = $date;
            }
        };
        $fullDates = array_merge_recursive(self::getDaysOff(), $fullDates);
        return $fullDates;
    }

    /**
     * {@inheritdoc}
     */
    public function checkAvailability($date, $qte = null){

        $SoldTickets = $this->getSoldTickets($order);

        $dispo = true;

        $date = \DateTime::createFromFormat('d/m/Y', $userChoices['date'])->format('Y-m-d');

        if(array_key_exists($date,$SoldTickets) == true){
            if($SoldTickets[$date] +  $userChoices['ticket_qte'] > $this->qteMax){
                $dispo = false;
            }
        }

        return $dispo;
    }
}