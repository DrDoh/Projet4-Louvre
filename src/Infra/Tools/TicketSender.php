<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 24/09/2018
 * Time: 15:42.
 */

namespace App\Infra\Tools;

use App\Infra\Tools\interfaces\TicketSenderInterface;
use Dompdf\Dompdf;
use Dompdf\Options;
use Swift_Mailer;
use Twig\Environment;


final class TicketSender implements TicketSenderInterface
{
    /**
     * @var Environment
     */
    private $env;

    /**
     * @var Swift_Mailer
     */
    private $swift_Mailer;

    /**
     * @var string
     */
    private $publicFolder;

    /**
     * {@inheritdoc}
     */
    public function __construct(
        Environment $env,
        Swift_Mailer $swift_Mailer,
        string $publicFolder
    ){
        $this->env = $env;
        $this->swift_Mailer = $swift_Mailer;
        $this->publicFolder = $publicFolder;
    }

    /**
     * {@inheritdoc}
     */
    public function sendByEmail($order, $pdf)
    {
        $message = new \Swift_Message();
        $imgUrl = $message->embed(\Swift_Image::fromPath($this->publicFolder.'img/global/logo-louvre.png'));

        $filename = "Le Louvre : Billet d'accées.pdf";
        $message->setFrom('ludovic.parhelia@gmail.com')
                ->setTo($order->getEmail())
                ->setSubject('Musée du Louvre : Vos billets')
                ->setBody(
                    $this->env->render(
                        'Email/ticket_email.html.twig',
                        [
                            'order' => $order,
                            'logo' => $imgUrl,
                        ]
                    ),
                    'text/html'
                );
        $attach = new \Swift_Attachment($pdf, $filename, 'application/pdf');
        $message->attach($attach);

        $this->swift_Mailer->send($message);
    }
}
