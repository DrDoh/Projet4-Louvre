<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 19/09/2018
 * Time: 11:06.
 */

namespace App\Infra\Doctrine\Repository\Interfaces;

use Doctrine\Common\Persistence\ManagerRegistry;

interface OrderRepositoryInterface
{
//    /**
//     * OrderRepositoryInterface constructor.
//     *
//     * @param ManagerRegistry $registry
//     */
//    public function __construct( ManagerRegistry $registry);

    /**
     * @param $order
     *
     * @return mixed
     */
    public function save($order);
}
