<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 18/09/2018
 * Time: 17:05.
 */

namespace App\Infra\Doctrine\Repository\Interfaces;

use App\Domain\Models\Interfaces\TicketInterface;
use Doctrine\Common\Persistence\ManagerRegistry;

interface TicketRepositoryInterface
{
    /**
     * TicketRepositoryInterface constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry);

    /**
     * @return mixed
     */
    public function getAllTickets();

    /**
     * @return mixed
     */
    public function getAllTicketsAfterNow();

    /**
     * @param TicketInterface $ticket
     *
     * @return mixed
     */
    public function save(TicketInterface $ticket);
}
