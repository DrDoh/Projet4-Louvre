<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 19/09/2018
 * Time: 11:04.
 */

namespace App\Infra\Doctrine\Repository;

use App\Domain\Models\Order;
use App\Infra\Doctrine\Repository\Interfaces\OrderRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

final class OrderRepository extends ServiceEntityRepository implements OrderRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    /**
     * {@inheritdoc}
     */
    public function getOrdersAfterNow()
    {
        return $this
            ->createQueryBuilder('t')
            ->where('t.visitDate >= :now')
            ->leftJoin('t.tickets', 'tt')
            ->setParameter('now', new \DateTime())
            ->getQuery()
            ->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function save($order)
    {
        $this->_em->persist($order);
        $this->_em->flush();
    }
}
