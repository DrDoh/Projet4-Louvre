<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 18/09/2018
 * Time: 17:03.
 */

namespace App\Infra\Doctrine\Repository;

use App\Domain\Models\Interfaces\TicketInterface;
use App\Domain\Models\Ticket;
use App\Infra\Doctrine\Repository\Interfaces\TicketRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

final class TicketRepository extends ServiceEntityRepository implements TicketRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ticket::class);
    }

    /**
     * {@inheritdoc}
     */
    public function getAllTickets()
    {
        return $this->createQueryBuilder('tickets')
                    ->getQuery()
                    ->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function getAllTicketsAfterNow()
    {
        $qb = $this->createQueryBuilder('t');
        $now = new \DateTime();
        $qb
            ->where('t.date < :now')
            ->setParameter('now', $now)
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function save(TicketInterface $ticket)
    {
        $this->_em->persist($this);
        $this->_em->flush();
    }
}
