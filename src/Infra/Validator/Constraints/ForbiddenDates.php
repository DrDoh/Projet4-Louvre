<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 19/10/2018
 * Time: 10:00
 */

namespace App\Infra\Validator\Constraints;


use App\Infra\Validator\Constraints\Interfaces\ForbiddenDatesInterface;
use Symfony\Component\Validator\Constraint;

final class ForbiddenDates extends Constraint implements ForbiddenDatesInterface
{

    public $message = 'order.forbidden_date';


}