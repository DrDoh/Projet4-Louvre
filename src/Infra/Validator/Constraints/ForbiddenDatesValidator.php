<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 19/10/2018
 * Time: 10:00
 */

namespace App\Infra\Validator\Constraints;


use App\Infra\Doctrine\Repository\Interfaces\OrderRepositoryInterface;
use App\Infra\Tools\interfaces\AvailabilityCheckerInterface;
use App\Infra\Validator\Constraints\Interfaces\ForbiddenDatesValidatorInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

final class ForbiddenDatesValidator extends ConstraintValidator implements ForbiddenDatesValidatorInterface
{

    private $availabilityChecker;

    public function __construct(AvailabilityCheckerInterface $availabilityChecker)
    {
        $this->availabilityChecker=$availabilityChecker;
    }

    public function validate($value, Constraint $constraint)
    {

        $today = new \DateTime();

        if(in_array($value->format('d/m/Y'),$this->availabilityChecker->getUnavailableDate()) || $value->format('D') === 'Tue' || $value->format('D') ==='Sun' || ($today->format('d/m/Y') > $value->format('d/m/Y'))){
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', $value->format('d/m/Y'))
                ->addViolation();
        }

    }
}