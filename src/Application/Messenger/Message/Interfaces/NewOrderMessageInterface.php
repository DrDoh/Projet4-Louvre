<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 11/10/2018
 * Time: 11:29.
 */

namespace App\Application\Messenger\Message\Interfaces;

use App\Domain\Models\Interfaces\OrderInterface;

interface NewOrderMessageInterface
{
    /**
     * NewOrderMessageInterface constructor.
     * @param OrderInterface $order
     */
    public function __construct(OrderInterface $order);

    /**
     * @return OrderInterface
     */
    public function getOrder(): OrderInterface;
}
