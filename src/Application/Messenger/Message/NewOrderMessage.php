<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 26/09/2018
 * Time: 15:38.
 */

namespace App\Application\Messenger\Message;

use App\Application\Messenger\Message\Interfaces\NewOrderMessageInterface;
use App\Domain\Models\Interfaces\OrderInterface;

final class NewOrderMessage implements NewOrderMessageInterface
{
    /**
     * @var OrderInterface
     */
    private $order;

    /**
     * {@inheritdoc}
     */
    public function __construct(OrderInterface $order)
    {
        $this->order = $order;
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder(): OrderInterface
    {
        return $this->order;
    }
}
