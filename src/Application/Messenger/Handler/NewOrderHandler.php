<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 26/09/2018
 * Time: 15:37.
 */

namespace App\Application\Messenger\Handler;

use App\Application\Messenger\Handler\Interfaces\NewOrderHandlerInterface;
use App\Application\Messenger\Message\NewOrderMessage;
use App\Infra\Tools\interfaces\TicketPdfGeneratorInterface;
use App\Infra\Tools\interfaces\TicketSenderInterface;

final class NewOrderHandler implements NewOrderHandlerInterface
{
    /**
     * @var TicketPdfGeneratorInterface
     */
    private $pdfGenerator;

    /**
     * @var TicketSenderInterface
     */
    private $ticketSender;

    /**
     * {@inheritdoc}
     */
    public function __construct(
        TicketPdfGeneratorInterface $pdfGenerator,
        TicketSenderInterface $ticketSender
    ){
        $this->pdfGenerator = $pdfGenerator;
        $this->ticketSender = $ticketSender;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(NewOrderMessage $message)
    {
        $pdf = $this->pdfGenerator->createPdf($message);
        $this->ticketSender->sendByEmail($message->getOrder(), $pdf);
    }
}
