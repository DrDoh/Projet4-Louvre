<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 11/10/2018
 * Time: 11:28.
 */

namespace App\Application\Messenger\Handler\Interfaces;

use App\Application\Messenger\Message\NewOrderMessage;
use App\Infra\Tools\interfaces\TicketPdfGeneratorInterface;
use App\Infra\Tools\interfaces\TicketSenderInterface;

interface NewOrderHandlerInterface
{
    /**
     * NewOrderHandlerInterface constructor.
     * @param TicketPdfGeneratorInterface $pdfGenerator
     * @param TicketSenderInterface $ticketSender
     */
    public function __construct(
        TicketPdfGeneratorInterface $pdfGenerator,
        TicketSenderInterface $ticketSender
    );

    /**
     * @param NewOrderMessage $message
     * @return mixed
     */
    public function __invoke(NewOrderMessage $message);
}
