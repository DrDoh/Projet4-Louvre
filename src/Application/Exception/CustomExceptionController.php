<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 12/12/2018
 * Time: 10:29
 */

namespace App\Application\Exception;


use App\Application\Exception\Interfaces\CustomExceptionControllerInterface;
use Symfony\Bundle\TwigBundle\Controller\ExceptionController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

final class CustomExceptionController extends ExceptionController implements CustomExceptionControllerInterface
{
    /**
     * {@inheritdoc}
     */
    public function __invoke(Request $request){
        return new RedirectResponse('/fr');
    }
}