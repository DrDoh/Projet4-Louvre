<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 12/12/2018
 * Time: 10:29
 */

namespace App\Application\Exception\Interfaces;


use Symfony\Component\HttpFoundation\Request;


interface CustomExceptionControllerInterface
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function __invoke(Request $request);
}