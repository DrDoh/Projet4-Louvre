formGenerator = {

    index: 0,
    inputMax: 5,
    addLinkText: '',
    deleteLinkText: '',
    visitorLabel: '',
    btnAddLink:'',
    btnDeleteLink:'',
    $container: null,

    init : function($container, addLinkText, deleteLinkText, visitorLabel)
    {
        this.$container = $container;
        this.addLinkText = addLinkText;
        this.deleteLinkText = deleteLinkText;
        this.visitorLabel = visitorLabel;

        this.btnAddLink = '<a href="#" class="add btn btn-info">' + this.addLinkText + ' <i class="fas fa-user-plus"></i></a>';
        this.btnDeleteLink = '<a href="#" class="delete btn btn-danger">'+ this.deleteLinkText +' <i class="fas fa-user-minus"></i></a>';

        this.index = this.$container.children('fieldset').length;

        if(this.index === 0 ){
            this.createNewFiedset();
        }else{
            this.updateFiedset(this.$container.children('fieldset'));
        }
    },

    createNewFiedset: function ()
    {
        let $prototype = this.createPrototype();
        this.$container.append($prototype);

        $('.js-datepicker').datepicker({
            format: 'dd/mm/yyyy',
            language: "fr",
            orientation: "auto top",
            autoclose: true,
            endDate: "now",
            startView: "decade"
        });

        return this.index++;
    },

    updateFiedset : function (fieldsets) {

        for(let i=0; i<fieldsets.length; i++)
        {
            $('.js-datepicker').datepicker({
                format: 'dd/mm/yyyy',
                language: "fr",
                orientation: "auto top",
                autoclose: true,
                endDate: "now",
                startView: "decade",
                disableTouchKeyboard: true
            });

            let $fieldset = $(fieldsets[i]);
            $fieldset.children('legend').text(this.visitorLabel + ' n°'+ (i+1)).attr('class','text-right font-weight-light');
            $fieldset.children('legend').prepend('<i class="far fa-user"></i> ');
            $fieldset.append(this.btnAddLink);
            this.addlink($fieldset);
            if(i>0){
                $fieldset.append(this.btnDeleteLink);
                this.deleteLink($fieldset);
            }


        }
    },

    createPrototype: function()
    {
        const template = this.$container.attr('data-prototype')
            .replace(/__name__label__/g, this.visitorLabel + ' n°' + ((this.index)+1))
            .replace(/__name__/g,        this.index);

        let $prototype = $(template).attr('id', 'fieldset_' + this.index);
        $prototype.children('legend').attr('class','text-right font-weight-light');
        $prototype.children('legend').prepend('<i class="far fa-user"></i> ');

        if(this.index < 1){
            $prototype.append(this.btnAddLink);
            this.addlink($prototype);
        } else {
            $prototype.append(this.btnAddLink);
            this.addlink($prototype);
            $prototype.append(this.btnDeleteLink);
            this.deleteLink($prototype);
        }
        return $prototype;
    }, 
    
    addlink: function ($fieldset) {

        let btn = $fieldset.children('.add');

        btn.click(function(e) {
            formGenerator.createNewFiedset();
            e.preventDefault();
            return false;
        });
    },
    
    deleteLink: function ($fieldset) {
        let btn = $fieldset.children('.delete');
        btn.click(function(e) {
            $fieldset.remove();
            e.preventDefault();
            return this.index--;
        });
    }

}


