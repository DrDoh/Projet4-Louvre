date_range_switch = {

    switchHour: 14,
    jQery_selector: '.js-datepicker',
    
    checkTime : function (switchHour) {
        let now = new Date();
        let h = now.getHours();
        let d = now.getDate();
        if (d < 10) {
            d = "0" + d;
        }
        let m = now.getMonth() + 1;
        if (m < 10) {
            m = "0" + m;
        }
        let y = now.getFullYear();
        let date = d + "/" + m + "/" + y;

        let chosen_date = $(this.jQery_selector).val();

        if(h>=switchHour && chosen_date === date){
            return true
        }
        return false
    },

    switch : function () {
        if(this.checkTime(this.switchHour)){
            $('#order_visitRange').val('halfDay');
            $('#fullDay').attr('selected',false);
            $('#fullDay').attr('disabled',true);
            $('#halfDay').attr('selected',true);
        }else{
            $('#order_visitRange').val('fullDay');
            $('#fullDay').attr('disabled',false);
            $('#halfDay').attr('selected',false);
        }
    }

}
