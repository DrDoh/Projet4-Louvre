<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 04/09/2018
 * Time: 10:49
 */

namespace App\Tests\UI\Responder;


use App\UI\Responder\Interfaces\PaymentActionResponderInterface;
use App\UI\Responder\PaymentActionResponder;
use PHPUnit\Framework\TestCase;
use Twig\Environment;

final class PaymentActionresponderUnitTest extends TestCase
{

    private $env = null;

    public function setUp()
    {
        $this->env = $this->createMock(Environment::class);
    }

    public function testItImplements()
    {
        $responder = new PaymentActionResponder($this->env);
        static::assertInstanceOf(PaymentActionResponderInterface::class, $responder);
    }
}