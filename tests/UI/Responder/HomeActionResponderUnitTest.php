<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 31/08/2018
 * Time: 15:18
 */

namespace App\Tests\UI\Responder;


use App\UI\Responder\HomeActionResponder;
use App\UI\Responder\Interfaces\HomeActionResponderInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

/**
 * Class HomeActionResponderUnitTest
 */
final class HomeActionResponderUnitTest extends TestCase
{

    private $env=null;

    private $generator=null;

    public function setUp()
    {
        $this->env = $this->createMock(Environment::class);
        $this->generator = $this->createMock(UrlGeneratorInterface::class);
    }

    public function testItImplements(){
        $responder = new HomeActionResponder($this->env, $this->generator);

        static::assertInstanceOf(HomeActionResponderInterface::class,$responder);
    }
}