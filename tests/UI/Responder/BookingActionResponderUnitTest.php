<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 03/09/2018
 * Time: 11:46
 */

namespace App\Tests\UI\Responder;


use App\UI\Responder\BookingActionResponder;
use App\UI\Responder\Interfaces\BookingActionResponderInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

/**
 * Class BookingActionResponderUnitTest
 */
final class BookingActionResponderUnitTest extends TestCase
{
    private $env = null;

    private $generator = null;

    public function setUp()
    {
        $this->env = $this->createMock(Environment::class);
        $this->generator = $this->createMock(UrlGeneratorInterface::class);
    }

    public function testItImplements()
    {
        $responder = new BookingActionResponder($this->env, $this->generator);

        static::assertInstanceOf(BookingActionResponderInterface::class, $responder);
    }
}