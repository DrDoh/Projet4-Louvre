<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 31/08/2018
 * Time: 13:43
 */

namespace App\Tests\UI\Action;


use App\Infra\Tools\interfaces\AvailabilityCheckerInterface;
use App\UI\Action\HomeAction;
use App\UI\Action\Interfaces\HomeActionInterface;
use App\UI\Form\Handler\Interfaces\OrderTypeHandlerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Form\FormFactoryInterface;

final class HomeActionUnitTest extends TestCase
{
    /**
     * @var FormFactoryInterface|null
     */
    private $formFactory = null;

    /**
     * @var OrderTypeHandlerInterface|null
     */
    private $formHandler = null;

    /**
     * @var AvailabilityCheckerInterface\null
     */
    private $availabilityChecker = null;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->formFactory = $this->createMock(FormFactoryInterface::class);
        $this->formHandler = $this->createMock(OrderTypeHandlerInterface::class);
        $this->availabilityChecker = $this->createMock(AvailabilityCheckerInterface::class);
    }

    public function testItImplements()
    {
        $action = new HomeAction($this->formFactory, $this->formHandler, $this->availabilityChecker);

        static::assertInstanceOf(HomeActionInterface::class,$action);
    }
}