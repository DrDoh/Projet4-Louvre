<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 03/09/2018
 * Time: 10:51
 */

namespace App\Tests\UI\Action;


use App\Infra\Tools\interfaces\AvailabilityCheckerInterface;
use App\UI\Action\BookingAction;
use App\UI\Action\Interfaces\BookingActionInterface;
use App\UI\Form\Handler\Interfaces\BookingTypeHandlerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Form\FormFactoryInterface;

final class BookingActionUnitTest extends TestCase
{

    /**
     * @var FormFactoryInterface|null
     */
    private $formFactory = null;

    /**
     * @var BookingTypeHandlerInterface|null
     */
    private $formHandler = null;

    /**
     * @var AvailabilityCheckerInterface\null
     */
    private $availabilityChecker = null;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->formFactory = $this->createMock(FormFactoryInterface::class);
        $this->formHandler = $this->createMock(BookingTypeHandlerInterface::class);
        $this->availabilityChecker = $this->createMock(AvailabilityCheckerInterface::class);
    }


        public function testItImplements()
    {
        $action = new BookingAction($this->formFactory, $this->formHandler, $this->availabilityChecker);

        static::assertInstanceOf(BookingActionInterface::class,$action);
    }
}