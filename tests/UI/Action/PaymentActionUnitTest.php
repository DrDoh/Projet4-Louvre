<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 04/09/2018
 * Time: 10:46
 */

namespace App\Tests\UI\Action;


use App\UI\Action\Interfaces\PaymentActionInterface;
use App\UI\Action\PaymentAction;
use App\UI\Form\Handler\Interfaces\StripeTypeHandlerInterface;
use PHPUnit\Framework\TestCase;

final class PaymentActionUnitTest extends TestCase
{

    /**
     * @var StripeTypeHandlerInterface
     */
    private $formHandler = null;


    public function setUp(){
        $this->formHandler = $this->createMock(StripeTypeHandlerInterface::class);
    }

    public function testItImplements(){
        $action = new PaymentAction($this->formHandler);

        static::assertInstanceOf(PaymentActionInterface::class, $action);
    }
}