<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 31/08/2018
 * Time: 09:34
 */

namespace App\Tests\UI\Form;


use App\Domain\DTO\interfaces\OrderDTOInterface;
use App\UI\Form\OrderType;
use App\UI\Form\Interfaces\OrderTypeInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Test\TypeTestCase;

final class OrderTypeUnitTest extends TypeTestCase
{
    public function testItImplements()
    {
        $type = new OrderType();

        static::assertInstanceOf(AbstractType::class, $type);
        static::assertInstanceOf(OrderTypeInterface::class, $type);
    }

    /**
     * @param $visitDate
     * @param $visitRange
     *
     * @dataProvider provideCredentials
     */
    public function testItAcceptData(string $visitDate, string $visitRange)
    {

        $type = $this->factory->create(OrderType::class);

        $type->submit([
            'visitDate' => $visitDate,
            'visitRange' => $visitRange
        ]);

        static::assertTrue($type->isSubmitted());
        static::assertTrue($type->isValid());
        static::assertInstanceOf(OrderDTOInterface::class, $type->getData());
        static::assertNotSame($visitDate, $type->getData()->visitDate);

        $newDate = new \DateTime();
        $dateTest = $newDate->createFromFormat('d/m/Y',$visitDate);
        $dateToTest = $dateTest->setTime(0,0,0,0);
        static::assertEquals($dateToTest, $type->getData()->visitDate);
        static::assertSame($visitRange, $type->getData()->visitRange);
    }

    /**
     * @return \Generator
     */
    public function provideCredentials()
    {
        yield array('31/12/2018', 'fullDay');
        yield array('30/06/2018', 'halfDay');
    }
}