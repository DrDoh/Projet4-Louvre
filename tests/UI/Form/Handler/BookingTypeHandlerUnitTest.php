<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 12/09/2018
 * Time: 10:20
 */

namespace App\Tests\UI\Form\Handler;


use App\UI\Form\Handler\BookingTypeHandler;
use App\UI\Form\Handler\Interfaces\BookingTypeHandlerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

final class BookingTypeHandlerUnitTest extends TestCase
{
    /**
     * @var SessionInterface
     */
    private $session = null;

    public function setUp(){
        $this->session = $this->createMock(SessionInterface::class);
    }

    public function testItImplements()
    {
        $handler = new BookingTypeHandler($this->session);

        static::assertInstanceOf(BookingTypeHandlerInterface::class, $handler);
    }
}