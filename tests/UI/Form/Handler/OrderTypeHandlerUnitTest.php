<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 31/08/2018
 * Time: 14:26
 */

namespace App\Tests\UI\Form\Handler;


use App\UI\Form\Handler\OrderTypeHandler;
use App\UI\Form\Handler\Interfaces\OrderTypeHandlerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

final class OrderTypeHandlerUnitTest extends TestCase
{

    /**
     * @var SessionInterface
     */
    private $session;

    public function setup()
    {
        $this->session = $this->createMock(SessionInterface::class);
    }

    public function testItImplements()
    {
        $handler = new OrderTypeHandler($this->session);

        static::assertInstanceOf(OrderTypeHandlerInterface::class, $handler);
    }
}