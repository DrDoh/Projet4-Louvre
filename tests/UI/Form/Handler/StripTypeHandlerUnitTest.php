<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 18/09/2018
 * Time: 10:57
 */

namespace App\Tests\UI\Form\Handler;


use App\Domain\Factory\Interfaces\OrderFactoryInterfaces;
use App\Domain\Factory\Interfaces\TicketFactoryInterface;
use App\Infra\Doctrine\Repository\Interfaces\OrderRepositoryInterface;
use App\Infra\Tools\interfaces\TicketSenderInterface;
use App\Infra\Tools\interfaces\StripeInterface;
use App\UI\Form\Handler\Interfaces\StripeTypeHandlerInterface;
use App\UI\Form\Handler\StripeTypeHandler;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class StripTypeHandlerUnitTest extends TestCase
{
    /**
     * @var MessageBusInterface
     */
    private $bus= null;

    /**
     * @var OrderFactoryInterfaces
     */
    private $orderFactory = null;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository = null;

    /**
     * @var SessionInterface
     */
    private $session = null;

    /**
     * @var TicketSenderInterface
     */
    private $sendTicket;

    /**
     * @var StripeInterface
     */
    private $stripe = null;

    /**
     * @var TicketFactoryInterface
     */
    private $ticketFactory= null;

    /**
     * @var ValidatorInterface
     */
    private $validator= null;


    public function setUp(){
        $this->bus = $this->createMock(MessageBusInterface::class);
        $this->orderFactory = $this->createMock(OrderFactoryInterfaces::class);
        $this->orderRepository = $this->createMock(OrderRepositoryInterface::class);
        $this->session = $this->createMock(SessionInterface::class);
        $this->stripe = $this->createMock(StripeInterface::class);
        $this->sendTicket = $this->createMock(TicketSenderInterface::class);
        $this->ticketFactory = $this->createMock(TicketFactoryInterface::class);
        $this->validator=$this->createMock(ValidatorInterface::class);
    }

    public function testItImplements(){
        $handler = new StripeTypeHandler(
            $this->bus,
            $this->orderFactory,
            $this->orderRepository,
            $this->sendTicket,
            $this->session,
            $this->stripe,
            $this->ticketFactory,
            $this->validator
        );

        static::assertInstanceOf(StripeTypeHandlerInterface::class, $handler);
    }
}