<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 28/08/2018
 * Time: 17:17
 */

namespace App\Tests\UI\Form;


use App\Domain\DTO\interfaces\TicketDTOInterface;
use App\UI\Form\TicketType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Test\TypeTestCase;
use App\UI\Form\Interfaces\TicketTypeInterface;

/**
 * Class TicketTypeUnitTest
 */
final class TicketTypeUnitTest extends TypeTestCase
{
    public function testImplements()
    {
        $type = new TicketType();

        static::assertInstanceOf(AbstractType::class, $type);
        static::assertInstanceOf(TicketTypeInterface::class, $type);
    }

    /**
     * @param string $lastName
     * @param string $firstName
     * @param string $birthdate
     * @param string $discount
     * @param string $country
     *
     * @dataProvider provideCredentials
     */
    public function testItAcceptData(string $lastName,string $firstName, string $birthdate, string $discount, string $country)
    {
        $type = $this->factory->create(TicketType::class);

        $type->submit([
            'lastName' => $lastName,
            'firstName' => $firstName,
            'birthdate' => $birthdate,
            'discount' => $discount,
            'country'=> $country
        ]);

        static::assertTrue($type->isSubmitted());
        static::assertTrue($type->isValid());
        static::assertInstanceOf(TicketDTOInterface::class, $type->getData());
        static::assertSame($lastName, $type->getData()->lastName);
        static::assertSame($firstName, $type->getData()->firstName);

        static::assertNotSame($birthdate, $type->getData()->birthdate);
        $newDate = new \DateTime();
        $dateTest = $newDate->createFromFormat('d/m/Y',$birthdate);
        $dateToTest = $dateTest->setTime(0,0,0,0);
        static::assertEquals($dateToTest, $type->getData()->birthdate);

        static::assertSame($discount, $type->getData()->discount);
        static::assertSame($country, $type->getData()->country);
    }

    /**
     * @return \Generator
     */
    public function provideCredentials()
    {
        yield array('lastName', 'firstName', '12/12/1988', 'none', 'FR');
    }
}