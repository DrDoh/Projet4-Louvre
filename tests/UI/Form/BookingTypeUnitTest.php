<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 06/09/2018
 * Time: 16:31
 */

namespace App\Tests\UI\Form;


use App\Domain\DTO\interfaces\BookingDTOInterface;
use App\UI\Form\BookingType;
use App\UI\Form\Interfaces\BookingTypeInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Test\TypeTestCase;

final class BookingTypeUnitTest extends TypeTestCase
{
    public function testItImplements()
    {
        $type = new BookingType();

        static::assertInstanceOf(AbstractType::class, $type);
        static::assertInstanceOf(BookingTypeInterface::class, $type);
    }

    /**
     * @param $tickets
     *
     * @dataProvider provideCredentials
     */
    public function testItAcceptData(array $tickets)
    {
        $type = $this->factory->create(BookingType::class);

        $type->submit([
            'tickets' => $tickets
        ]);

        static::assertTrue($type->isSubmitted());
        static::assertTrue($type->isValid());
    }

    /**
     * @return \Generator
     */
    public function provideCredentials()
    {
        yield array( array('firstName', 'lastName', 'birthdate', 'discount', 'country'));
    }
}
