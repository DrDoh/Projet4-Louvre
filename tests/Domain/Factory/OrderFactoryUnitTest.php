<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 20/09/2018
 * Time: 12:09
 */

namespace App\Tests\Domain\Factory;


use App\Domain\DTO\OrderDTO;
use App\Domain\Factory\Interfaces\OrderFactoryInterfaces;
use App\Domain\Factory\Interfaces\TicketFactoryInterface;
use App\Domain\Factory\OrderFactory;
use App\Domain\Models\Interfaces\OrderInterface;
use DateTime;
use PHPUnit\Framework\TestCase;

final class OrderFactoryUnitTest extends TestCase
{
    /**
     * @var $ticketFactory
     */
    private $ticketFactory;


    public function setUp(){

        $this->ticketFactory = $this->createMock(TicketFactoryInterface::class);
    }


    public function testItImplements()
    {
        $factory = new OrderFactory($this->ticketFactory);

        static::assertInstanceOf(OrderFactoryInterfaces::class, $factory);
    }

    /**
     * @param $order
     *
     * @dataProvider provideCredentials
     */
    public function testItCreate($order)
    {
        $factory = new OrderFactory($this->ticketFactory);

        $order = $factory->createFromUI($order);

        static::assertInstanceOf(OrderInterface::class, $order);
    }

    public function provideCredentials()
    {
        $tickets = [];

        $order = new OrderDTO(new DateTime(),'halfDay', $tickets, 'email');

        yield array($order);
    }
}