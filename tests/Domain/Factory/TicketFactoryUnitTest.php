<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 20/09/2018
 * Time: 12:34
 */

namespace App\Tests\Domain\Factory;


use App\Domain\DTO\TicketDTO;
use App\Domain\Factory\Interfaces\TicketFactoryInterface;
use App\Domain\Factory\TicketFactory;
use App\Domain\Models\Interfaces\TicketInterface;
use DateTime;
use PHPUnit\Framework\TestCase;

final class TicketFactoryUnitTest extends TestCase
{
    public function testItImplements()
    {
        $factory = new TicketFactory();

        static::assertInstanceOf(TicketFactoryInterface::class, $factory);
    }

    /**
     * @param $tickets
     *
     * @dataProvider provideCredentials
     */
    public function testItCreate($tickets)
    {
        $factory = new TicketFactory();

        $tickets = $factory->createFromUI($tickets);

        static::assertInstanceOf(TicketInterface::class, $tickets[0]);
    }

    public function provideCredentials()
    {
        $ticket = new TicketDTO(new DateTime(),"","","","");
        $ticket->price = 10;
        $ticket->type = '';
        $tickets[] = $ticket;
        yield array($tickets);
    }
}