<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 03/09/2018
 * Time: 14:12
 */

namespace App\Tests\Domain\DTO;


use App\Domain\DTO\interfaces\TicketDTOInterface;
use App\Domain\DTO\TicketDTO;
use PHPUnit\Framework\TestCase;

final class TicketDTOUnitTest extends TestCase
{
    /**
     * @param \DateTimeInterface $birthdate
     * @param string $country
     * @param string $discount
     * @param string $firstName
     * @param string $lastName
     *
     * @dataProvider provideCredentials
     */
    public function testItImplements(
        \DateTimeInterface $birthdate,
        string $country,
        string $discount,
        string $firstName,
        string $lastName
    ){
        $dto = new TicketDTO(
            $birthdate,
            $country,
            $discount,
            $firstName,
            $lastName
        );

        static::assertInstanceOf(TicketDTOInterface::class, $dto);

        static::assertSame($dto->lastName, $lastName);
        static::assertSame($dto->firstName, $firstName);
        static::assertSame($dto->birthdate, $birthdate);
        static::assertSame($dto->discount, $discount);
        static::assertSame($dto->country, $country);
    }

    public function provideCredentials()
    {
        yield array(new \DateTime(),'FR', 'none', 'fisrtName','lastName');
    }
}