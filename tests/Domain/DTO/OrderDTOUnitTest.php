<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 31/08/2018
 * Time: 12:10
 */

namespace App\Tests\Domain\DTO;


use App\Domain\DTO\interfaces\OrderDTOInterface;
use App\Domain\DTO\OrderDTO;
use PHPUnit\Framework\TestCase;

final class OrderDTOUnitTest extends TestCase
{
    /**
     * @param \DateTimeInterface $visitDate
     * @param string $visiteRange
     *
     * @dataProvider provideCredentials
     */
    public function testItImplements(\DateTime $visitDate, string $visiteRange)
    {
        $dto = new OrderDTO($visitDate, $visiteRange);

        static::assertInstanceOf(OrderDTOInterface::class, $dto);
        static::assertSame($dto->visitDate,$visitDate);
        static::assertSame($dto->visitRange,$visiteRange);
    }

    /**
     * @return \Generator
     */
    public function provideCredentials()
    {
        yield array(new \DateTime(),'visit range');
    }
}