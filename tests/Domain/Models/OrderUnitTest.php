<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 28/08/2018
 * Time: 15:38
 */

namespace App\Tests\Domain\Models;


use App\Domain\Models\Interfaces\OrderInterface;
use App\Domain\Models\Order;
use ArrayAccess;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

/**
 * Class OrderUnitTest
 * @package App\Tests\Domain\Models
 */
final class OrderUnitTest extends TestCase
{
    /**
     * @param string $email
     * @param string $orderId
     * @param \DateTimeInterface $creationDate
     * @param \DateTimeInterface $visitDate
     * @param string $visitRange
     * @param array $tickets
     *
     * @throws \Exception
     * @dataProvider provideCredentials
     */
    public function testItImplements(
        \DateTimeInterface $creationDate,
        string $email,
        string $orderId,
        \DateTimeInterface $visitDate,
        string $visitRange
    ){
        $order= new Order(
            $creationDate,
            $email,
            $orderId,
            $visitDate,
            $visitRange
        );

        static::assertInstanceOf(OrderInterface::class, $order);
        static ::assertSame($order->getEmail(),$email);
        static ::assertSame($order->getorderId(),$orderId);
        static ::assertSame($order->getCreationDate(),$creationDate);
        static ::assertSame($order->getVisitDate(),$visitDate);
        static ::assertSame($order->getVisitRange(),$visitRange);

    }

    public function provideCredentials(){
        yield array(new \DateTime(),'email','order id', new \DateTime(),'visit range');
    }
}
