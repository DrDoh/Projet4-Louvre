<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 28/08/2018
 * Time: 16:25
 */

namespace App\Tests\Domain\Models;


use App\Domain\Models\Interfaces\TicketInterface;
use App\Domain\Models\Ticket;
use DateTime;
use DateTimeInterface;
use PHPUnit\Framework\TestCase;

final class TicketUnitTest extends TestCase
{
    /**
     * @param DateTimeInterface $creationDate
     * @param string $lastName
     * @param string $firstName
     * @param string $country
     * @param DateTimeInterface $birthdate
     * @param string $ticketId
     * @param string $ticketType
     * @param int $price
     *
     * @dataProvider provideCredentials
     * @throws \Exception
     */
    public function testImplements(
        \DateTimeInterface $birthdate,
        \DateTimeInterface $creationDate,
        string $country,
        string $firstName,
        string $lastName,
        int $price,
        string $ticketId,
        string $ticketType
    ){
        $ticket = new Ticket(
            $birthdate,
            $creationDate,
            $country,
            $firstName,
            $lastName,
            $price,
            $ticketId,
            $ticketType
        );

        static::assertInstanceOf(TicketInterface::class, $ticket);

        static ::assertSame($ticket->getCreationDate(),$creationDate);
        static ::assertSame($ticket->getLastName(),$lastName);
        static ::assertSame($ticket->getFirstName(),$firstName);
        static ::assertSame($ticket->getCountry(),$country);
        static ::assertSame($ticket->getBirthdate(),$birthdate);
        static ::assertSame($ticket->getTicketId(),$ticketId);
        static ::assertSame($ticket->getTicketType(),$ticketType);
        static ::assertSame($ticket->getPrice(),$price);
    }

    public function provideCredentials(){
        yield array(
            new DateTime(),
            new DateTime(),
            'pays',
            'Prénom',
            'Nom',
            10,
            'id du billet',
            'type de billet'
        );
    }
}