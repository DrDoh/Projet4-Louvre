<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 11/10/2018
 * Time: 14:30
 */

namespace App\Tests\Application\Messenger\Handler;


use App\Application\Messenger\Handler\Interfaces\NewOrderHandlerInterface;
use App\Application\Messenger\Handler\NewOrderHandler;
use App\Infra\Tools\interfaces\TicketPdfGeneratorInterface;
use App\Infra\Tools\interfaces\TicketSenderInterface;
use App\Infra\Tools\TicketPdfGenerator;
use Knp\Snappy\Pdf;
use PHPUnit\Framework\TestCase;
use Twig\Environment;

final class NewOrderHandlerUnitTest extends TestCase
{
    /**
     * @var TicketPdfGeneratorInterface|null
     */
    private $pdfGenerator =null;
    /**
     * @var TicketSenderInterface|null
     */
    private $sendTicket = null;


    public function setUp()
    {
        $this->sendTicket =$this->createMock(TicketSenderInterface::class);
        $this->pdfGenerator =$this->createMock(TicketPdfGeneratorInterface::class);
    }

    public function testItImplements()
    {
        $newOrderHandler = new NewOrderHandler($this->pdfGenerator, $this->sendTicket);

        static::assertInstanceOf( NewOrderHandlerInterface::class, $newOrderHandler);
    }
}