<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 11/10/2018
 * Time: 14:31
 */

namespace App\Tests\Application\Messenger\Message;


use App\Application\Messenger\Message\Interfaces\NewOrderMessageInterface;
use App\Application\Messenger\Message\NewOrderMessage;
use App\Domain\Models\Interfaces\OrderInterface;
use PHPUnit\Framework\TestCase;

final class NewOrderMessageUnitTest extends  TestCase
{
    private $order;

    public function setUp()
    {
        $this->order = $this->createMock(OrderInterface::class);
    }

    public function testItImplement()
    {

        $newOrderMessage = new NewOrderMessage($this->order);
        static::assertInstanceOf(NewOrderMessageInterface::class, $newOrderMessage);
    }
}