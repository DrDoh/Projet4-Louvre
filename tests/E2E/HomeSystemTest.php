<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 25/10/2018
 * Time: 16:38
 */

namespace App\Tests\E2E;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

final class HomeSystemTest extends WebTestCase
{
    public function testStatusCodeFr()
    {
        $client = static::createClient();

        $client->request('GET', '/fr');

        static::assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());

    }

    public function testStatusCodeEn()
    {
        $client = static::createClient();

        $client->request('GET', '/en');

        static::assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    public function testStatusCodeWithoutLocal()
    {
        $client = static::createClient();

        $client->request('GET', '/');

        static::assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    public function testStatusCodeWithFalseUrl()
    {
        $client = static::createClient();

        $client->request('GET', '/azdaz');

        static::assertSame(Response::HTTP_FOUND, $client->getResponse()->getStatusCode());

        $client->followRedirect();

        static::assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        static::assertSame($client->getRequest()->getRequestUri(), '/fr');
    }
}