<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 27/11/2018
 * Time: 09:59
 */

namespace App\Tests\E2E;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

final class BookingSystemTest extends WebTestCase
{
    public function testStatusCodeFr()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/fr');

        $form = $crawler->selectButton('Suivant')->form();

        $form['order[visitDate]'] = '28/12/2018';
        $form['order[visitRange]'] = 'fullDay';

        $client->submit($form);

        $client->followRedirect();

        static::assertSame($client->getRequest()->getRequestUri(), '/fr/billetterie');
    }

    public function testStatusCodeEn()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/en');

        $form = $crawler->selectButton('Next')->form();

        $form['order[visitDate]'] = '28/12/2018';
        $form['order[visitRange]'] = 'fullDay';

        $client->submit($form);

        $client->followRedirect();

        static::assertSame($client->getRequest()->getRequestUri(), '/en/billetterie');
    }

    public function testWithWrongDate()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/fr');

        $form = $crawler->selectButton('Suivant')->form();

        $form['order[visitDate]'] = '25/12/2020';
        $form['order[visitRange]'] = 'fullDay';

        $client->submit($form);

        static::assertSame($client->getRequest()->getRequestUri(), '/fr');
    }

    public function testWithOldDate()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/fr');

        $form = $crawler->selectButton('Suivant')->form();

        $form['order[visitDate]'] = '10/05/2017';
        $form['order[visitRange]'] = 'fullDay';

        $client->submit($form);

        static::assertSame($client->getRequest()->getRequestUri(), '/fr');
    }

    public function testDirectAccessFr()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/fr/billetterie');

        $client->followRedirect();

        static::assertSame($client->getRequest()->getRequestUri(), '/fr');
    }

}