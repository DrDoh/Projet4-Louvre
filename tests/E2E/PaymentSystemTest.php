<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 27/11/2018
 * Time: 10:59
 */

namespace App\Tests\E2E;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class PaymentSystemTest extends WebTestCase
{

    public function testDirectAccess()
    {
        $client = static::createClient();

        $client->request('GET', '/fr/payement');

        $client->followRedirect();

        static::assertSame($client->getRequest()->getRequestUri(), '/fr');
    }

    public function testRightAccess()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/fr');

        $form = $crawler->selectButton('Suivant')->form();

        $form['order[visitDate]'] = '14/12/2018';
        $form['order[visitRange]'] = 'fullDay';

        $client->submit($form);

        $client->followRedirect();

        $crawler = $client->getCrawler();

        $form2 = $crawler->filter('button')->form();

        $values = $form2->getPhpValues();

        $values['booking']['tickets'][0]['firstName'] = 'Name';
        $values['booking']['tickets'][0]['lastName'] = 'LastName';
        $values['booking']['tickets'][0]['birthdate'] = '13/12/1988';
        $values['booking']['tickets'][0]['discount'] = 'discount';
        $values['booking']['tickets'][0]['country'] = 'FR';

        $client->submit($form2);

        static::assertSame(Response::HTTP_FOUND, $client->getResponse()->getStatusCode());

        $client->followRedirect();

        static::assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        static::assertSame($client->getRequest()->getRequestUri(), '/fr/payement');

    }

}