<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 18/09/2018
 * Time: 11:31
 */

namespace App\Tests\Infra\Tools;


use App\Domain\DTO\OrderDTO;
use App\Infra\Tools\interfaces\PriceCalculatorInterface;
use App\Infra\Tools\PriceCalculator;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class PriceCalUnitTest extends KernelTestCase
{

    private $fullPrice;
    private $childPrice;
    private $babyPrice;
    private $seniorPrice;
    private $discountPrice;
    private $childMaxAge;
    private $childMinAge;
    private $seniorMinAge;

    public function setUp(){
        $kernel = self::bootKernel();
        $container = $kernel->getContainer();
        $this->fullPrice = $container->getParameter('full_price');
        $this->childPrice = $container->getParameter('child_price');
        $this->babyPrice = $container->getParameter('baby_price');
        $this->seniorPrice = $container->getParameter('senior_price');
        $this->discountPrice = $container->getParameter('discount_price');
        $this->childMinAge = $container->getParameter('child_max_age');
        $this->childMaxAge = $container->getParameter('child_min_age');
        $this->seniorMinAge = $container->getParameter('senior_min_age');
    }

    public function testConstruct(){
        $priceCal = new PriceCalculator(
            $this->fullPrice,
            $this->childPrice,
            $this->babyPrice,
            $this->seniorMinAge,
            $this->discountPrice,
            $this->childMaxAge,
            $this->childMinAge,
            $this->seniorMinAge
        );

        static::assertInstanceOf(PriceCalculatorInterface::class, $priceCal);
    }

//    public function testUpdateDto()
//    {
//
//    }

}