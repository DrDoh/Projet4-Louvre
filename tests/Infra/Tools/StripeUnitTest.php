<?php
/**
 * Created by PhpStorm.
 * User: parhelia3
 * Date: 18/09/2018
 * Time: 11:32
 */

namespace App\Tests\Infra\Tools;


use App\Infra\Tools\interfaces\StripeInterface;
use App\Infra\Tools\Stripe;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class StripeUnitTest extends KernelTestCase
{

    private $stripeSecretKey;

    public function setUp()
    {

        $this->stripeSecretKey = 'Stripe Secret Key';
    }

    public function testConstruct(){

        $stripe = new Stripe($this->stripeSecretKey);

        static::assertInstanceOf(StripeInterface::class, $stripe);
    }

//    public function TestPayement()
//    {
//
//    }

}