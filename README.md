To run this projet you need to run docker on your computer. If it’s already install, you can skip the first step. 

1. Install docker : follow the instruction on this page => https://docs.docker.com

2. From the console
   - Create a new folder (mkdir …) then get in the new folder (cd …)

   - Git clone :  https://gitlab.com/DrDoh/Projet4-Louvre.git

   - make start 

   - make install

   - make schema-create
		
3. Create and set a « .env » file ( you can use the .env.dist as a template) 
